# -*- coding: utf-8 -*-
import clipboard
import binascii



def run_clipboard_tests():
    def test_empty_clipboard():
        clipboard.reset()
	assert(clipboard.gettext() == None)
        assert(clipboard.getblob() == None)
    
    def test_reset_clipboard():
        clipboard.reset()
        assert(clipboard.gettext() == None)
        assert(clipboard.getblob() == None)
    
    def test_copy_hindi_text():
        clipboard.reset()
        msg = u'विकिपीडिया:इण्टरनेट पर हिन्दी के साधन'
        clipboard.copytext(msg)
        text = clipboard.gettext()
        assert(msg == text)


    def test_copy_english_text():
        clipboard.reset()
        clipboard.copytext("hello, world!",15)
        text = clipboard.gettext()
	assert(text == "hello, world!")

    def test_copy_blob():
        clipboard.reset()
        clipboard.copyblob([1,0,1,0,1,0,1],10000)
        blob = clipboard.getblob()
	assert(blob == [1,0,1,0,1,0,1])
    
    test_copy_blob()
    test_empty_clipboard()
    test_reset_clipboard()
    test_copy_english_text()

run_clipboard_tests()

def run_clipboard_state_tests():
    def convert_blob_to_text(blobpath):
    	file = open(blobpath,"rb") 
	with file:
		byte = file.read()
		hexadecimal = binascii.hexlify(byte)
  		decimal = int(hexadecimal, 16)
		binary = bin(decimal)[2:].zfill(12123)
	#	print binary
		return binary
    
    def both_empty():
        assert(clipboard.gettext() == None)
        assert(clipboard.getblob() == None)
    def copyblob_from_empty():
        clipboard.reset()
	bina=convert_blob_to_text("b.jpg")
        clipboard.copyblob(convert_blob_to_text("b.jpg"),12312312312312213)
	assert(clipboard.getblob() == bina)
	assert(clipboard.gettext() == None)
    def copytext_from_empty():
        clipboard.reset()
        clipboard.copytext("1,2,3,4",12)
        assert(clipboard.gettext() == "1,2,3,4")
        assert(clipboard.getblob() == None)
    def copyblob_from_copytext():
	txt=clipboard.gettext()
	bina=convert_blob_to_text("b.jpg")
	clipboard.copyblob(convert_blob_to_text("b.jpg"),12312312312312213)
	assert(clipboard.getblob() == bina)
	assert(clipboard.gettext() == txt)
    def copytext_from_copyblob():
	blb=clipboard.getblob()
        clipboard.copytext("1,3,4,5",213)
        assert(clipboard.getblob() == blb)
        assert(clipboard.gettext() == "1,3,4,5")
    clipboard.reset()
    both_empty()
    copyblob_from_empty()
    clipboard.reset()
    copytext_from_empty()
    clipboard.reset()
    clipboard.copytext("[1,3,4]",123123)
    copyblob_from_copytext()
    clipboard.reset()
    bina=convert_blob_to_text("b.jpg")
    clipboard.copyblob(bina,12312312312312213)
    copytext_from_copyblob()
    
run_clipboard_state_tests()

def run_clipboard_observer_tests():
    def test_one_observer():
        def anobserver(reason):
            print "observer notified. reason: ", reason

        clipboard.reset()
        test_one_observer()
        run_clipboard_observer_tests()
        clipboard.addobserver(anobserver)
  
text=clipboard.__text__
v=clipboard.copytext("hello, world!",10)
assert(clipboard.get_file_text(v)=="hello, world!")
assert(text==clipboard.__text__)

