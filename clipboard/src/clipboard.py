import sys
import os
import tempfile
__all__ = ["copytext", "copyblob", "gettext", "getblob", "reset"]

__text__ = None
__blob__ = None
__size__ = None
__limit__ = None

def copytext(text,size):
    global __text__
    if len(text) < size :
        __text__ = text
	return None
    else:
	temp=tempfile.mkstemp()
#	print temp
	fil=os.open(temp[1],os.O_RDWR|os.O_CREAT)
	os.write(fil, text)
	os.close( fil )
	return temp[1]
	#raise Exception("Text stored till size (limit.")

def get_file_text(path):
    foo = open(path, "r")	    
    hello=foo.read()
    return hello

def copyblob(blob,size):
    global __blob__
    if len(blob) < size:
        __blob__ = blob
    else:
        temp=tempfile.TemporaryFile()
        try:
            temp.write(text)
            temp.seek(0)
            print temp.read()
        finally:
            temp.close()

        #raise Exception("Blob stored till size limit.")


def gettext():
    global __text__
    return __text__

def getblob():
    global __blob__
    return __blob__

def reset():
    global __text__, __blob__
    __text__ = None
    __blob__ = None
    __size__ = None



##
## -------------------------------------------------------------
##
__observers__ = []
def addobserver(observer):
    __observers__.append(observer)

def removeobserver(observer):
    try:
        __observers__.remove(observer)
    except ValueError, TypeError:
        pass

def notify(reason):
    for observer in __observers__:
        if observer is not None:
            try:
                observer(reason)
            except TypeError:
                pass

