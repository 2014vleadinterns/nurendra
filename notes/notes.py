import re
import glob
import json
import cPickle
from xml.etree.ElementTree import Element, SubElement, Comment, tostring
__notes__=[]

def new_note(title1,body):
    global __notes__
    title='Un1titl;ed' if title1 == None else title1
    matching=[s for s in __notes__ if title in s[0]]
    __notes__.append((title,body,len(matching)))    

def find_notes_title(key1):
    global __notes__
    key="Un1titl;ed" if key1 == None else key1
    allsubs = [x for x in __notes__ if re.search(key , x[0])]
    return allsubs

def find_notes_body(key):
    global __notes__
    allsubs = [x for x in __notes__ if re.search(key , x[1])]
    return allsubs
	    
def delete_node_by_title(title1,index):
    global __notes__
    title='Un1titl;ed' if title1 == None else title1
    matching=[s for s in __notes__ if title==s[0] and s[2]==index]
    __notes__.remove(matching[0])

def delete_all_node_by_title(title1):
    global __notes__
    title='Un1titl;ed' if title1 == None else title1
    matching=[s for s in __notes__ if title==s[0]]
    [__notes__.remove(x) for x in matching]

def save_notes_json():
    global __notes__
    json.dump(__notes__,open("save.json",'w+'))

def get_saved_notes_json():
    json.load(open("save.json",'rb'))
    return json.load(open("save.json",'rb'))    

def save_notes_pickle():
    global __notes__
    cPickle.dump( __notes__, open( "save.p", "wb" ) )
	
def get_saved_notes_pickle():
    return cPickle.load( open( "save.p", "rb" ) )

def get_notes():
    global __notes__
    return __notes__

def reset_notes():
    global __notes__
    __notes__=[] 


































"""
new_note("asda","12312344")
new_note("asda","12431414555")
new_note("asdfgfg","insfjksbfkjbsghjbdfjbgsbgsj")
print "Notes:",__notes__
print find_notes("asd")
print find_notes("asdf")
delete_node_by_title("asda",1)
print "Notes:",__notes__
"""	

 


