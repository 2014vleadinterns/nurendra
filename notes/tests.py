from notes import *

def run_notes_tests():
    def test_reset_notes():
    	reset_notes()
    	assert(get_notes()==[])
    
    def test_new_note():
    	new_note("cool","123123123")  
    	new_note(None,"11231233312")
    	new_note("cool","12313233")
    	new_note(None,"12312312 321333")
    	new_note("cool","12313233")
    	new_note("hot","sadasdasd")
    	assert(get_notes()==[("cool","123123123",0),("Un1titl;ed","11231233312",0),("cool","12313233",1),("Un1titl;ed","12312312 321333",1),("cool","12313233",2),("hot","sadasdasd",0)])
    
    def test_save_notes_json():
	save_notes_json()

    def test_get_saved_notes_json():
        get_saved_notes_json() 
    
    def test_save_notes_pickle():
	save_notes_pickle()

    def test_get_saved_notes_pickle():
	get_saved_notes_pickle()

    def test_find_notes_title():
	assert(find_notes_title("co")==[("cool","123123123",0),("cool","12313233",1),("cool","12313233",2)])
    	assert(find_notes_title(None)==[("Un1titl;ed","11231233312",0),("Un1titl;ed","12312312 321333",1)])	
	assert(find_notes_title("cool123")==[])	
    
    def test_find_notes_body():
	assert(find_notes_body("312 321")==[("Un1titl;ed","12312312 321333",)])
	assert(find_notes_body(None)==None)

    def test_delete_node_by_title():
    	delete_node_by_title("cool",0)
    	assert(get_notes()==[("Un1titl;ed","11231233312",0),("cool","12313233",1),("Un1titl;ed","12312312 321333",1),("cool","12313233",2),("hot","sadasdasd",0)])
    	delete_node_by_title(None,1) 
    	assert(get_notes()==[("Un1titl;ed","11231233312",0),("cool","12313233",1),("cool","12313233",2),("hot","sadasdasd",0)])
    
    def test_delete_all_node_by_title():
    	delete_all_node_by_title("cool")
	assert(get_notes()==[("Un1titl;ed","11231233312",0),("hot","sadasdasd",0)]) 
    test_reset_notes()
    test_new_note()
    test_find_notes_title()
    test_delete_node_by_title()
    test_save_notes_json()
    test_get_saved_notes_json()
    test_save_notes_pickle()
    test_get_saved_notes_pickle()
    test_reset_notes()
    assert(find_notes_title(None)==[])
    assert(find_notes_body("sdasd")==[])			

run_notes_tests()
		
