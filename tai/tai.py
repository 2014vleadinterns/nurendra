import sys
fil=open(sys.argv[1],"r")
thefile=fil.read()
v=thefile.splitlines()

def check_length(instr):
    return len(instr)==4

def check_operator(instr):
    return instr[0]=='*' or instr[0]=='+' or instr[0]=='-' or instr[0]=='/' or instr[0]=='%'

def check_operand1(instr):
    return (all(i.isalnum() or i=='_' for i in instr[1])) and (not instr[1][0].isdigit() or instr[1].isdigit())

def check_operand2(instr):
    return (all(i.isalnum() or i=='_' for i in instr[2])) and (not instr[2][0].isdigit() or instr[2].isdigit())

def check_result(instr):
    return (0 if instr[3].isdigit() and (not((instr[0]=='+' and int(instr[1])+int(instr[2])==int(instr[3])) or (instr[0]=='-' and int(instr[1])-int(instr[2])==int(instr[3])) or (instr[0]=='*' and (int(instr[1])*int(instr[2])==int(instr[3]))) or (instr[0]=='/' and int(instr[1])/int(instr[2])==int(instr[3])) or (instr[0]=='%' and int(instr[1])%int(instr[2])==int(instr[3])))) else 1) if instr[1].isdigit() and instr[2].isdigit() else (all(i.isalnum() or i=='_' for i in instr[3]))

count=1
for i in v[0:-3]:
        man=i.split()
        a,b,c,d,e=check_length(man),check_operator(man),check_operand1(man),check_operand2(man),check_result(man)	
	error="Error : Line"+str(count)+(": Check length." if not a else ": Check operator." if not b else ": Check the 1st operand." if not c else ": Check the 2nd operand." if not d else ": Wrong Result" if not e else "")
	if not(a and b and c and d and e): print error 
	count+=1
     
    

	
    
    
    
